package security.oauth2.controller;


import lombok.SneakyThrows;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.endpoint.TokenEndpoint;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.security.Principal;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author zheng
 */
@RestController
@RequestMapping("oauth")
public class OauthController {

    /**
     * 返回增强
     */
    @Resource
    private TokenEndpoint tokenEndpoint;


    @PostMapping("token")
    public Map<String, Object> postAccessToken(Principal principal, @RequestParam Map<String,String> param) throws HttpRequestMethodNotSupportedException {
        Map<String, Object> map = custom(tokenEndpoint.postAccessToken(principal, param).getBody());
        return map;
    }

    private Map<String,Object> custom(OAuth2AccessToken oAuth2AccessToken){
        DefaultOAuth2AccessToken token = (DefaultOAuth2AccessToken) oAuth2AccessToken;

        Map<String,Object> data =  new LinkedHashMap(token.getAdditionalInformation());
        data.put("accessToken",token.getValue());
        data.put("expireIn", token.getExpiresIn());
        data.put("scopes", token.getScope());
        if (token.getRefreshToken() != null){
            data.put("refreshToken",token.getRefreshToken().getValue());
        }
        return data;
    }

}
