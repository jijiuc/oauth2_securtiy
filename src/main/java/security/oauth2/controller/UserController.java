package security.oauth2.controller;

import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 用户中心
 * @author zheng
 */
@RestController
public class UserController {

    @GetMapping("user/me")
    public Object postAccessToken(Authentication authentication){
        Object principal = authentication.getPrincipal();
        return principal;
    }
}
