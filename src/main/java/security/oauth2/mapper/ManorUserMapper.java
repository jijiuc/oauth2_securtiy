package security.oauth2.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import security.oauth2.entity.ManorUser;

public interface ManorUserMapper extends BaseMapper<ManorUser> {
}
