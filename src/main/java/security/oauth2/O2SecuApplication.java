package security.oauth2;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Slf4j
@SpringBootApplication
@MapperScan("security.oauth2.mapper")
public class O2SecuApplication {

    public static void main(String[] args) {
        SpringApplication.run(O2SecuApplication.class, args);
    }

}
