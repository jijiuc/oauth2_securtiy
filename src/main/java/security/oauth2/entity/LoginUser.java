package security.oauth2.entity;

import org.springframework.security.core.userdetails.UserDetails;

import com.baomidou.mybatisplus.annotation.TableId;
import com.google.common.collect.Lists;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author zheng
 */
@Getter
@Setter
public class LoginUser implements UserDetails {

    /**
     *
     */
    @TableId
    private Integer id;
    /**
     * 是否需要引导（默认为0，需要 1：不需要）
     */
    private Integer guideType;
    /**
     * 等级
     */
    private Integer level;
    /**
     * 用户经验值
     */
    private Integer experience;

    /**
     * 种植水果类型 （默认0：未种  1.桃树，2.梨树，3.西瓜，4.草莓，5.葡萄）
     */
    private Integer fruitsType;

    /**
     * 果树等级
     */
    private Integer fruitsLevel;
    /**
     * 果树经验值
     */
    private Integer fruitsExp;
    /**
     * 用户id
     */
    private Integer userId;

    /**
     * 弹幕开启：0 关闭 1：开启
     */
    private Integer setBulletArr;

    private Date createTime;

    private Integer state;

    /**
     * 肥料
     */
    private Integer manureNum;

    /**
     * 水
     */
    private Integer waterNum;

    /**
     * 加速卡
     */
    private Integer cardNum;

    /**
     * 当月前用户签到总数
     */
    private Integer addSign;

    private String logo;

    private String username;

    private String password;

    private String roles;

    private List<GrantedAuthority> authorities;

    //** 获取角色信息
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        if (StringUtils.isNotBlank(this.roles)){
            //获取数据库中角色
            Lists.newArrayList();
            this.authorities = Stream.of(this.roles.split(",")).map(role ->{
                return new SimpleGrantedAuthority(role);
            }).collect(Collectors.toList());
        }else{
            // 如果角色为空
            this.authorities = AuthorityUtils
                    .commaSeparatedStringToAuthorityList("ROLE_USER");
        }
        return this.authorities;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return this.state == 0 ? false : true;
    }
}

