/*
 * Copyright (c) 2018-2999 宿迁广电庄园 All rights reserved.
 *
 * https://www.gz-yami.com/
 *
 */

package security.oauth2.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 广电庄园 —— 用户信息表
 *
 * @author LGH
 * @date 2021-05-11 17:26:24
 */
@Data
@TableName("manor_user")
public class ManorUser implements Serializable{
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId
    private Integer id;
    /**
     * 是否需要引导（默认为0，需要 1：不需要）
     */
    private Integer guideType;
    /**
     * 等级
     */
    private Integer level;
    /**
     * 用户经验值
     */
    private Integer experience;

    /**
     * 种植水果类型 （默认0：未种  1.桃树，2.梨树，3.西瓜，4.草莓，5.葡萄）
     */
    private Integer fruitsType;

    /**
     * 果树等级
     */
    private Integer fruitsLevel;
    /**
     * 果树经验值
     */
    private Integer fruitsExp;
    /**
     * 用户id
     */
    private Integer userId;

    /**
     * 弹幕开启：0 关闭 1：开启
     */
    private Integer setBulletArr;

    private Date createTime;

    private Integer state;

    /**
     * 肥料
     */
    private Integer manureNum;

    /**
     * 水
     */
    private Integer waterNum;

    /**
     * 加速卡
     */
    private Integer cardNum;

    /**
     * 当月前用户签到总数
     */
    private Integer addSign;

    private String logo;

    private String username;

    private String password;

}
