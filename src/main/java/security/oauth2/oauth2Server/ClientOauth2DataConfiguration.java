package security.oauth2.oauth2Server;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 客户端数据配置类
 * @author zheng
 */
@Component
@ConfigurationProperties(prefix = "client.oauth2")
@Data
public class ClientOauth2DataConfiguration {


    private String clientId;

    private String secret;

    private String[] grantTypes;

    private int tokenValidityTime;

    private int refreshTokenValidityTime;

    private String[] scopes;
}
