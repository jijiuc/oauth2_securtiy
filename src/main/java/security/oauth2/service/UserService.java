package security.oauth2.service;

import com.baomidou.mybatisplus.extension.service.IService;
import security.oauth2.entity.ManorUser;

public interface UserService extends IService<ManorUser> {
}
