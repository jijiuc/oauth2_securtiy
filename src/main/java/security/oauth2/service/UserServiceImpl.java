package security.oauth2.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import security.oauth2.entity.LoginUser;
import security.oauth2.entity.ManorUser;
import security.oauth2.mapper.ManorUserMapper;

@Service
public class UserServiceImpl extends ServiceImpl<ManorUserMapper, ManorUser> implements UserService, UserDetailsService {

    @Autowired
    private ManorUserMapper userMapper;

    /**
     * Security 用户登陆实现方法
     * @param username
     * @return
     * @throws UsernameNotFoundException
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        ManorUser user = userMapper.selectOne(new LambdaQueryWrapper<ManorUser>().eq(ManorUser::getUserId, username));
        LoginUser loginUser = new LoginUser();
        // 复制用户信息
        BeanUtils.copyProperties(user,loginUser);
//        return new User(username,user.getPassword(), AuthorityUtils.commaSeparatedStringToAuthorityList(null));
        return loginUser;
    }
}
