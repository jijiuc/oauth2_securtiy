/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50725
 Source Host           : localhost:3306
 Source Schema         : yami_shops

 Target Server Type    : MySQL
 Target Server Version : 50725
 File Encoding         : 65001

 Date: 24/08/2021 00:33:39
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for manor_user
-- ----------------------------
DROP TABLE IF EXISTS `manor_user`;
CREATE TABLE `manor_user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `guide_type` tinyint(1) DEFAULT 0 COMMENT '是否需要引导（默认为0，需要 1：不需要）',
  `level` int(255) DEFAULT 1 COMMENT '用户等级',
  `experience` int(255) DEFAULT 0 COMMENT '用户经验值',
  `fruits_type` tinyint(1) DEFAULT 0 COMMENT '种植水果类型 （默认0：未种  1.桃树，2.梨树，3.西瓜，4.草莓，5.葡萄）',
  `fruits_exp` int(255) DEFAULT 0 COMMENT '果树经验值',
  `user_id` int(20) NOT NULL COMMENT '用户id',
  `fruits_level` tinyint(2) DEFAULT 0 COMMENT '果树等级',
  `set_bullet_arr` tinyint(1) DEFAULT 1 COMMENT '弹幕开启：0 关闭 1：开启',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `state` tinyint(1) DEFAULT 1 COMMENT '状态：1 启用 0 禁用',
  `manure_num` int(11) DEFAULT 0 COMMENT '肥料',
  `water_num` int(11) DEFAULT 0 COMMENT '水',
  `card_num` int(11) DEFAULT NULL COMMENT '加速卡',
  `add_sign` int(255) DEFAULT 0 COMMENT '本月前用户签到总数',
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '用户名',
  `logo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '头像',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '密码',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `user_id`(`id`, `user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 20210513 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '广电庄园 —— 用户信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of manor_user
-- ----------------------------
INSERT INTO `manor_user` VALUES (20210511, 1, 30, 1268710, 1, 71120, 10148629, 7, 0, '2021-06-02 11:39:33', 1, 245, 910, 2, 7, '抽奖专用小马甲', 'http://logo.sqsjt.net/upfiles/user/face/90/9005c1be3839c52c52c97e7589e46d5c.jpg?5F1FF05C', 'e10adc3949ba59abbe56e057f20f883e');
INSERT INTO `manor_user` VALUES (20210512, 1, 4, 1815, 4, 1690, 10108103, 2, 1, '2021-08-06 15:19:24', 1, 1450, 1490, 0, 0, '开开', 'http://logo.sqsjt.net/upfiles/user/face/90/9005c1be3839c52c52c97e7589e46d5c.jpg?5F1FF05C', NULL);

SET FOREIGN_KEY_CHECKS = 1;
